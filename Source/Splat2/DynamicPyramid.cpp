// Fill out your copyright notice in the Description page of Project Settings.

#include "DynamicPyramid.h"
#include "ProceduralMeshComponent.h"

// Sets default values
ADynamicPyramid::ADynamicPyramid()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	RootComponent = mesh;
}

void ADynamicPyramid::PostActorCreated()
{
	Super::PostActorCreated();
	GenerateMesh();
}

void ADynamicPyramid::PostLoad()
{
	Super::PostLoad();
	GenerateMesh();
}

void ADynamicPyramid::GenerateMesh()
{
	TArray < FVector > Vertices;
	TArray < FVector > Normals;
	TArray < FProcMeshTangent > Tangents;
	TArray < FVector2D > TextureCoordinates;
	TArray < int32 > Triangles;
	TArray < FColor > Colors;

	CreateMesh(FVector(50, 50, 50), Vertices, Triangles, Normals, TextureCoordinates, Tangents, Colors);
	// Create the mesh section, specifying collision.  
	mesh->CreateMeshSection(0, Vertices, Triangles, Normals, TextureCoordinates, Colors, Tangents, true);
}

void ADynamicPyramid::CreateMesh(FVector Radius, TArray<FVector>& Vertices,
	TArray<int32>& Triangles, TArray<FVector>& Normals, TArray<FVector2D>& UVs,
	TArray<FProcMeshTangent>& Tangents, TArray<FColor>& Colors)
{
	FVector ZeroRadius(0, 0, 0);

	// Creates vertex location and orientation
	FVector TargetVerts[5];
	TargetVerts[0] = FVector(-Radius.X, -Radius.Y, ZeroRadius.Z); // front left
	TargetVerts[1] = FVector(ZeroRadius.X, ZeroRadius.Y, Radius.Z); // top
	TargetVerts[2] = FVector(-Radius.X, Radius.Y, ZeroRadius.Z); // front right
	TargetVerts[3] = FVector(Radius.X, -Radius.Y, ZeroRadius.Z); // back left
	TargetVerts[4] = FVector(Radius.X, Radius.Y, ZeroRadius.Z); // back right

	// generate triangles
	Triangles.Reset();
	const int32 NumVerts = 18; // 5 faces 3 verts per upper face, 4 on bottom face
	Colors.Reset();
	Colors.AddUninitialized(NumVerts);

	// sets color for vertices
	for (int i = 0; i < NumVerts / 3; i++)
	{
		Colors[i * 3] = FColor(255, 0, 0);
		Colors[i * 3 + 1] = FColor(0, 255, 0);
		Colors[i * 3 + 2] = FColor(0, 0, 255);
	}

	Vertices.Reset();
	Vertices.AddUninitialized(NumVerts);
	Normals.Reset();
	Normals.AddUninitialized(NumVerts);
	Tangents.Reset();
	Tangents.AddUninitialized(NumVerts);

	// Create first side
	// create vertices
	Vertices[0] = TargetVerts[0];
	Vertices[1] = TargetVerts[1];
	Vertices[2] = TargetVerts[2];

	// Assign vertices to index location for triangle
	Triangles.Add(1);
	Triangles.Add(0);
	Triangles.Add(2);

	// Set normal and tangent direction
	Normals[0] = Normals[1] = Normals[2] = FVector(-.5f, 0.f, .5f);
	Tangents[0] = Tangents[1] = Tangents[2] = FProcMeshTangent(0.f, -1.f, 0.f);

	//Create 2nd side
	Vertices[3] = TargetVerts[3];
	Vertices[4] = TargetVerts[4];
	Vertices[5] = TargetVerts[1];

	// Assign vertices to index location for triangle
	Triangles.Add(5);
	Triangles.Add(4);
	Triangles.Add(3);

	// Set normal and tangent direction
	Normals[3] = Normals[4] = Normals[5] = FVector(0.5f, 1.f, -0.5f);
	Tangents[3] = Tangents[4] = Tangents[5] = FProcMeshTangent(-1.f, 0.f, 0.f);

	// Create 3rd side
	Vertices[6] = TargetVerts[0];
	Vertices[7] = TargetVerts[3];
	Vertices[8] = TargetVerts[1];

	// Assign vertices to index location for triangle
	Triangles.Add(8);
	Triangles.Add(7);
	Triangles.Add(6);

	// Set normal and tangent direction
	Normals[6] = Normals[7] = Normals[8] = FVector(-0.5f, 0.f, -0.5f);
	Tangents[6] = Tangents[7] = Tangents[8] = FProcMeshTangent(0.f, 1.f, 0.f);

	// Create 4th side
	Vertices[9] = TargetVerts[4];
	Vertices[10] = TargetVerts[2];
	Vertices[11] = TargetVerts[1];

	// Assign vertices to index location for triangle
	Triangles.Add(11);
	Triangles.Add(10);
	Triangles.Add(9);

	// Set normal and tangent direction
	Normals[9] = Normals[10] = Normals[11] = FVector(0.5f, 0.f, 0.5f);
	Tangents[9] = Tangents[10] = Tangents[11] = FProcMeshTangent(0.f, 1.f, 0.f);

	// create bottom face
	Vertices[12] = TargetVerts[3];
	Vertices[13] = TargetVerts[4];
	Vertices[14] = TargetVerts[2];
	Vertices[15] = TargetVerts[3];
	Vertices[16] = TargetVerts[2];
	Vertices[17] = TargetVerts[0];

	// Assign vertices to index location for triangle
	Triangles.Add(12);
	Triangles.Add(13);
	Triangles.Add(14);
	Triangles.Add(15);
	Triangles.Add(16);
	Triangles.Add(17);


	// Set normal and tangent direction
	Normals[12] = Normals[13] = Normals[14] = Normals[15] = FVector(0.f, 0.5f, 0.5f);
	Tangents[12] = Tangents[13] = Tangents[14] = Tangents[15] = FProcMeshTangent(0.f, 1.f, 0.f);

	// Sets UV coords
	UVs.Reset();
	UVs.AddUninitialized(NumVerts);
	//UVs[0] = UVs[5] = UVs[10] = FVector2D(0.5f, 1.f);
	UVs[0] = UVs[3] = UVs[6] = UVs[9] = UVs[12] = UVs[15] = FVector2D(0.f, 1.f);
	UVs[1] = UVs[4] = UVs[7] = UVs[10] = UVs[13] = UVs[16] = FVector2D(0.f, 0.f);
	UVs[2] = UVs[5] = UVs[8] = UVs[11] = UVs[14] = UVs[17] = FVector2D(1.f, 0.5f);

	/*UVs[12] = FVector2D(0.f, 0.0f);
	UVs[13] = FVector2D(1.f, 0.f);
	UVs[14] = FVector2D(1.f, 1.f);
	UVs[15] = FVector2D(0.f, 1.f);*/
	//UVs[4] = UVs[9] = UVs[14]= FVector2D(.5f, 1.f);
}

// Called when the game starts or when spawned
void ADynamicPyramid::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADynamicPyramid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


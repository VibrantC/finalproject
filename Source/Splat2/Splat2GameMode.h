// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Splat2GameMode.generated.h"

UCLASS(minimalapi)
class ASplat2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASplat2GameMode();
};




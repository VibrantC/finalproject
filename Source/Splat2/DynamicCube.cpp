// Fill out your copyright notice in the Description page of Project Settings.

#include "DynamicCube.h"



// Sets default values
ADynamicCube::ADynamicCube()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	RootComponent = root;

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cube Mesh"));
	mesh->SetupAttachment(RootComponent);
	mesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	mesh->OnComponentHit.AddDynamic(this, &ADynamicCube::OnHit);

	effectAmount = 1.f;
}

void ADynamicCube::OnHit(UPrimitiveComponent * HitComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
	/*if (effectAmount > 0)
	{
		effectAmount -= .25;
	}
	else
	{
		effectAmount += .25;
	}

	UMaterialInstanceDynamic* changeMaterialInstance = mesh->CreateDynamicMaterialInstance(0);

	if (changeMaterialInstance != nullptr)
	{
		changeMaterialInstance->SetScalarParameterValue(FName("effectAmount"), effectAmount);
	}*/

}

// Called when the game starts or when spawned
void ADynamicCube::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ADynamicCube::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

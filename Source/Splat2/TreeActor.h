// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "ProceduralMeshComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TreeActor.generated.h"

UCLASS()
class SPLAT2_API ATreeActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATreeActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void PostActorCreated() override;
	virtual void PostLoad() override;

	/*virtual void GenerateMesh();
	virtual void CreateCoverObject(FVector BoxRadius, TArray < FVector > & Vertices,
		TArray < int32 > & Triangles, TArray < FVector > & Normals, TArray < FVector2D > & UVs,
		TArray < FProcMeshTangent > & Tangents, TArray < FColor > & Colors);*/

private:
	class UProceduralMeshComponent* mesh;

};

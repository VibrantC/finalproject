// Fill out your copyright notice in the Description page of Project Settings.

#include "ForestController.h"
#include "Splat2.h"


// Sets default values
AForestController::AForestController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// instantiate values
	newTreeX = 0.f;
	newTreeY = 0.f;
	newRockX = 0.f;
	newRockY = 0.f;
	newFoliageX = 0.f;
	newFoliageY = 0.f;
	newCubeX = 0.f;
	newCubeY = 0.f;

	// set up arrays
	oldTreeX.SetNum(200);
	oldTreeY.SetNum(200);
	oldRockX.SetNum(50);
	oldRockY.SetNum(50);
	oldFoliageX.SetNum(300);
	oldFoliageY.SetNum(300);
	oldCubeX.SetNum(10);
	oldCubeY.SetNum(10);
}

// Called when the game starts or when spawned
void AForestController::BeginPlay()
{
	Super::BeginPlay();
	
	// driver function
	for (int32 i = 0; i < 200; i++)
	{
		// generate x and y
		newTreeX = generateX(newTreeX);
		newTreeY = generateY(newTreeY);

		// Add x and y to the arrays
		oldTreeX[i] = newTreeX;
		oldTreeY[i] = newTreeY;

		// Check if the world exists
		const UWorld* world = GetWorld();

		if (world)
		{
			// create spawn params
			FActorSpawnParameters spawnParams;
			spawnParams.Owner = this;
			spawnParams.Instigator = Instigator;

			// Create a vector based on our code
			FVector newVec;
			newVec.Set(newTreeX, newTreeY, 110.f);

			// Spawn the object
			SpawnTrees(spawnParams, newVec);
		}
	}
	// driver function
	for (int32 i = 0; i < 300; i++)
	{
		// generate x and y
		newFoliageX = generateX(newTreeX);
		newFoliageY = generateY(newTreeY);

		// Add x and y to the arrays
		oldFoliageX[i] = newFoliageX;
		oldFoliageY[i] = newFoliageY;

		// Check if the world exists
		const UWorld* world = GetWorld();

		if (world)
		{
			// create spawn params
			FActorSpawnParameters spawnParams;
			spawnParams.Owner = this;
			spawnParams.Instigator = Instigator;

			// Create a vector based on our code
			FVector newVec;
			newVec.Set(newFoliageX, newFoliageY, 110.f);

			// Spawn the object
			SpawnFoliage(spawnParams, newVec);
		}
	}
	// driver function
	for (int32 i = 0; i < 50; i++)
	{
		// generate x and y
		newRockX = generateX(newRockX);
		newRockY = generateY(newRockY);

		// Add x and y to the arrays
		oldRockX[i] = newRockX;
		oldRockY[i] = newRockY;

		// Check if the world exists
		const UWorld* world = GetWorld();

		if (world)
		{
			// create spawn params
			FActorSpawnParameters spawnParams;
			spawnParams.Owner = this;
			spawnParams.Instigator = Instigator;

			// Create a vector based on our code
			FVector newVec;
			newVec.Set(newRockX, newRockY, 110.f);

			// Spawn the object
			SpawnRocks(spawnParams, newVec);
		}

		//// driver function
		//for (int32 i = 0; i < 10; i++)
		//{
		//	// generate x and y
		//	newCubeX = generateX(newCubeX);
		//	newCubeY = generateY(newCubeY);

		//	// Add x and y to the arrays
		//	oldCubeX[i] = newCubeX;
		//	oldCubeY[i] = newCubeY;

		//	// Check if the world exists
		//	const UWorld* world = GetWorld();

		//	if (world)
		//	{
		//		// create spawn params
		//		FActorSpawnParameters spawnParams;
		//		spawnParams.Owner = this;
		//		spawnParams.Instigator = Instigator;

		//		// Create a vector based on our code
		//		FVector newVec;
		//		newVec.Set(newCubeX, newCubeY, 110.f);

		//		// Spawn the object
		//		SpawnCubes(spawnParams, newVec);
		//	}
		//}
	}
}

// Called every frame
void AForestController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

float AForestController::generateX(float oldUsedX)
{
	float passBack = 0.f;

	passBack = FMath::RandRange(-5000, 5000);

	passBack = passBack + oldUsedX;

	if (passBack < -5000.f || passBack > 5000.f)
	{
		passBack = generateX(oldUsedX);
	}

	if (!isUsed(oldTreeX, oldRockX, oldFoliageX, passBack))
	{
		passBack = generateX(oldUsedX);
	}

	return passBack;
}

float AForestController::generateY(float oldUsedY)
{	
	float passBack = 0.f;

	passBack = FMath::RandRange(-5000, 5000);

	passBack = passBack + oldUsedY;

	if (passBack < -5000.f || passBack > 5000.f)
	{
		passBack = generateY(oldUsedY);
	}

	if (!isUsed(oldTreeY, oldRockY, oldFoliageY, passBack))
	{
		passBack = generateX(oldUsedY);
	}

	return passBack;
}

// Checks to see if it's been used
bool AForestController::isUsed(TArray<float>& oldTreeArray, TArray<float>& oldRockArray, TArray<float>& oldFoliageArray, float numCheck)
{
	bool flag = true;

	for (int32 i = 0; i < oldTreeArray.Num(); i++)
	{
		if (distanceBetween(oldTreeArray[i], numCheck) < 10.f)
		{
			flag = false;
		}
	}
	for (int32 i = 0; i < oldRockArray.Num(); i++)
	{
		if (distanceBetween(oldRockArray[i], numCheck) < 10.f)
		{
			flag = false;
		}
	}
	for (int32 i = 0; i < oldFoliageArray.Num(); i++)
	{
		if (distanceBetween(oldFoliageArray[i], numCheck) < 10.f)
		{
			flag = false;
		}
	}

	return flag;
}

float AForestController::distanceBetween(float inOne, float inTwo)
{
	float passBack = 0.f;

	bool flag = isOneGreater(inOne, inTwo);

	if (flag)
	{
		passBack = inOne - inTwo;
	}
	else
	{
		passBack = inTwo - inOne;
	}

	return passBack;
}

bool AForestController::isOneGreater(float inOne, float inTwo)
{
	bool flag = true;

	if (inOne <= inTwo)
	{
		flag = false;
	}

	return flag;
}

void AForestController::SpawnTrees(FActorSpawnParameters spawnParams, FVector newVec)
{
	int num = FMath::RandRange(1, 5);
	AActor * newActor;
	if (num == 1)
	{
		newActor = GetWorld()->SpawnActor<ATreeActor>(TreeOne, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 2)
	{
		newActor = GetWorld()->SpawnActor<ATreeActor>(TreeTwo, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 3)
	{
		newActor = GetWorld()->SpawnActor<ATreeActor>(TreeThree, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 4)
	{
		newActor = GetWorld()->SpawnActor<ATreeActor>(TreeFour, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 5)
	{
		newActor = GetWorld()->SpawnActor<ATreeActor>(TreeFive, newVec, FRotator::ZeroRotator, spawnParams);
	}
}

void AForestController::SpawnRocks(FActorSpawnParameters spawnParams, FVector newVec)
{
	int num = FMath::RandRange(1, 7);
	AActor * newActor;
	if (num == 1)
	{
		newActor = GetWorld()->SpawnActor<ARockActor>(ScreeOne, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 2)
	{
		newActor = GetWorld()->SpawnActor<ARockActor>(ScreeTwo, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 3)
	{
		newActor = GetWorld()->SpawnActor<ARockActor>(ScreeThree, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 4)
	{
		newActor = GetWorld()->SpawnActor<ARockActor>(ScreeFour, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 5)
	{
		newActor = GetWorld()->SpawnActor<ARockActor>(ScreeFive, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 6)
	{
		newActor = GetWorld()->SpawnActor<ARockActor>(ScreeSix, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 7)
	{
		newActor = GetWorld()->SpawnActor<ARockActor>(ScreeSeven, newVec, FRotator::ZeroRotator, spawnParams);
	}
}

void AForestController::SpawnFoliage(FActorSpawnParameters spawnParams, FVector newVec)
{
	int num = FMath::RandRange(1, 7);
	AActor * newActor;
	if (num == 1)
	{
		newActor = GetWorld()->SpawnActor<AFoliageActor>(FoliageOne, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 2)
	{
		newActor = GetWorld()->SpawnActor<AFoliageActor>(FoliageTwo, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 3)
	{
		newActor = GetWorld()->SpawnActor<AFoliageActor>(FoliageThree, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 4)
	{
		newActor = GetWorld()->SpawnActor<AFoliageActor>(FoliageFour, newVec, FRotator::ZeroRotator, spawnParams);
	}
	else if (num == 5)
	{
		newActor = GetWorld()->SpawnActor<AFoliageActor>(FoliageFive, newVec, FRotator::ZeroRotator, spawnParams);
	}
}

//void AForestController::SpawnCubes(FActorSpawnParameters spawnParams, FVector newVec)
//{
//	AActor* newActor = GetWorld()->SpawnActor<ADynamicCube>(cubeActor, newVec, FRotator::ZeroRotator, spawnParams);
//}

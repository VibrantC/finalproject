// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Engine.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DynamicCube.generated.h"

UCLASS()
class SPLAT2_API ADynamicCube : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADynamicCube();

	UPROPERTY(VisibleAnywhere)
		USceneComponent* root;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* mesh;

	UPROPERTY(EditAnywhere)
		UBoxComponent* trigger;

private:
	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	float effectAmount;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


private:
	class FPixelShaderUsageExample * PixelShading;
	class FComputeShaderUsageExample * ComputeShading;
	float EndColorBuildup;
	float EndColorBuildupDirection;
	float ComputeShaderBlendScalar;
	float ComputeShaderBlend;
	float TotalElapsedTime;
	void ModifyComputeShaderBlend(float NewScalar);
	void SavePixelShaderOutput();
	void SaveComputeShaderOutput();

	
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "ProceduralMeshComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DynamicPyramid.generated.h"

UCLASS()
class SPLAT2_API ADynamicPyramid : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADynamicPyramid();

	virtual void PostActorCreated() override;
	virtual void PostLoad() override;
	virtual void GenerateMesh();
	virtual void CreateMesh(FVector Radius, TArray<FVector>& Vertices,
		TArray<int32>& Triangles, TArray<FVector>& Normals, TArray<FVector2D>& UVs,
		TArray<FProcMeshTangent>& Tangents, TArray<FColor>& Colors);

private:
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent* mesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};

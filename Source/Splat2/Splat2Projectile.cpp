// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Splat2Projectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Engine.h"
#include "Components/DecalComponent.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"

ASplat2Projectile::ASplat2Projectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &ASplat2Projectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 5000.f;
	ProjectileMovement->MaxSpeed = 5000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

	static ConstructorHelpers::FObjectFinder<UMaterial>Material(TEXT("Material'/Game/PaintSplatter/Materials/M_Splats.M_Splats'"));
	DynamicMaterial = Material.Object;
}

void ASplat2Projectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
	}

	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
	{
		Decal = GetWorld()->SpawnActor<ADecalActor>(Hit.Location, FRotator());

		// Attach the material as a decal to the hit object
		if (Decal) {
			matInstance = UMaterialInstanceDynamic::Create(DynamicMaterial, this);
			matInstance->SetScalarParameterValue(FName("Frame"), FMath::RandRange(0, 3));
			matInstance->SetVectorParameterValue(FName("Color"), FLinearColor::Green);
			Decal->SetDecalMaterial(matInstance);
			Decal->SetActorRotation(UKismetMathLibrary::MakeRotFromX(Hit.Normal));
			Decal->SetLifeSpan(2.0f);
			Decal->GetDecal()->DecalSize = FVector(FMath::RandRange(12.f, 64.f), FMath::RandRange(12.f, 64.f), FMath::RandRange(12.f, 64.f));
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Red, TEXT("Decal Failed to Spawn"));
		}
	}
		Destroy();
	
}
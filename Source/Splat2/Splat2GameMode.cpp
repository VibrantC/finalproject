// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Splat2GameMode.h"
#include "Splat2HUD.h"
#include "Splat2Character.h"
#include "UObject/ConstructorHelpers.h"

ASplat2GameMode::ASplat2GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASplat2HUD::StaticClass();
}

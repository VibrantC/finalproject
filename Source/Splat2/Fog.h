// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Components/InstancedStaticMeshComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Fog.generated.h"

UCLASS()
class SPLAT2_API AFog : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFog();

	virtual void PostInitializeComponents() override;

	// set the fog size
	void setSize(float s);

	// reveal a portion of the fog
	void revealSmoothCircle(const FVector2D & pos, float radius);

private:
	void UpdateTextureRegions(UTexture2D* Texture, int32 MipIndex, uint32 NumRegions,
		FUpdateTextureRegion2D* Regions, uint32 SrcPitch, uint32 SrcBpp, uint8* SrcData, bool bFreeData);

	// fog texture size
	static const int m_textureSize = 512;

	UPROPERTY()
		UStaticMeshComponent* m_squarePlane;
	UPROPERTY()
		UTexture2D* m_dynamicTexture;
	UPROPERTY()
		UMaterialInterface* m_dynamicMaterial;
	UPROPERTY()
		UMaterialInstanceDynamic* m_dynamicMaterialInstance;

	uint8 m_pixelArray[m_textureSize* m_textureSize];

	FUpdateTextureRegion2D m_wholeTextureRegion;

	float m_coverSize;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "TreeActor.h"
#include "RockActor.h"
#include "FoliageActor.h"
#include "DynamicCube.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ForestController.generated.h"

UCLASS()
class SPLAT2_API AForestController : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AForestController();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// generate x and y coordinates to use
	float generateX(float oldUsedX);
	float generateY(float oldUsedY);

	// checks if the value has been used before by testing old array
	bool isUsed(TArray<float> & oldTreeArray, TArray<float>& oldRockArray, TArray<float>& oldFoliageArray, float numCheck);

	// checks the distance between points
	float distanceBetween(float inOne, float inTwo);

	// checks if one value is greater than another
	bool isOneGreater(float inOne, float inTwo);

	// spawns tree actors
	void SpawnTrees(FActorSpawnParameters spawnParams, FVector newVec);

	// spawns rock actors
	void SpawnRocks(FActorSpawnParameters spawnParams, FVector newVec);

	// spawns foliage actors
	void SpawnFoliage(FActorSpawnParameters spawnParams, FVector newVec);

	// spawn pyramid actors
	void SpawnCubes(FActorSpawnParameters spawnParams, FVector newVec);

	TArray<float> oldTreeX;
	TArray<float> oldTreeY;
	TArray<float> oldRockX;
	TArray<float> oldRockY;
	TArray<float> oldFoliageX;
	TArray<float> oldFoliageY;
	TArray<float> oldCubeX;
	TArray<float> oldCubeY;

	float newTreeX;
	float newTreeY;
	float newRockX;
	float newRockY;
	float newFoliageX;
	float newFoliageY;
	float newCubeX;
	float newCubeY;

protected:
	//Tree Actors
	UPROPERTY(EditDefaultsOnly, Category = "Tree one")
	TSubclassOf<ATreeActor> TreeOne;

	UPROPERTY(EditDefaultsOnly, Category = "Tree Two")
	TSubclassOf<ATreeActor> TreeTwo;

	UPROPERTY(EditDefaultsOnly, Category = "Tree Three")
	TSubclassOf<ATreeActor> TreeThree;

	UPROPERTY(EditDefaultsOnly, Category = "Tree Four")
	TSubclassOf<ATreeActor> TreeFour;

	UPROPERTY(EditDefaultsOnly, Category = "Tree Five")
	TSubclassOf<ATreeActor> TreeFive;

	// Rock Actors
	UPROPERTY(EditDefaultsOnly, Category = "Scree one")
	TSubclassOf<ARockActor> ScreeOne;

	UPROPERTY(EditDefaultsOnly, Category = "Scree two")
	TSubclassOf<ARockActor> ScreeTwo;

	UPROPERTY(EditDefaultsOnly, Category = "Scree three")
	TSubclassOf<ARockActor> ScreeThree;

	UPROPERTY(EditDefaultsOnly, Category = "Scree four")
	TSubclassOf<ARockActor> ScreeFour;

	UPROPERTY(EditDefaultsOnly, Category = "Scree Five")
	TSubclassOf<ARockActor> ScreeFive;

	UPROPERTY(EditDefaultsOnly, Category = "Scree six")
	TSubclassOf<ARockActor> ScreeSix;

	UPROPERTY(EditDefaultsOnly, Category = "Scree seven")
	TSubclassOf<ARockActor> ScreeSeven;

	// Foliage Actors
	UPROPERTY(EditDefaultsOnly, Category = "Foliage one")
	TSubclassOf<AFoliageActor> FoliageOne;

	UPROPERTY(EditDefaultsOnly, Category = "Foliage two")
	TSubclassOf<AFoliageActor> FoliageTwo;

	UPROPERTY(EditDefaultsOnly, Category = "Foliage three")
	TSubclassOf<AFoliageActor> FoliageThree;

	UPROPERTY(EditDefaultsOnly, Category = "Foliage four")
	TSubclassOf<AFoliageActor> FoliageFour;

	UPROPERTY(EditDefaultsOnly, Category = "Foliage five")
	TSubclassOf<AFoliageActor> FoliageFive;

	UPROPERTY(EditDefaultsOnly, Category = "Cube")
	TSubclassOf<ADynamicCube> cubeActor;
};
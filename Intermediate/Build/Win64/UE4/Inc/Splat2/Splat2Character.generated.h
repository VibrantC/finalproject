// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLAT2_Splat2Character_generated_h
#error "Splat2Character.generated.h already included, missing '#pragma once' in Splat2Character.h"
#endif
#define SPLAT2_Splat2Character_generated_h

#define Splat2_Source_Splat2_Splat2Character_h_15_RPC_WRAPPERS
#define Splat2_Source_Splat2_Splat2Character_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Splat2_Source_Splat2_Splat2Character_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASplat2Character(); \
	friend SPLAT2_API class UClass* Z_Construct_UClass_ASplat2Character(); \
public: \
	DECLARE_CLASS(ASplat2Character, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splat2"), NO_API) \
	DECLARE_SERIALIZER(ASplat2Character) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splat2_Source_Splat2_Splat2Character_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASplat2Character(); \
	friend SPLAT2_API class UClass* Z_Construct_UClass_ASplat2Character(); \
public: \
	DECLARE_CLASS(ASplat2Character, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splat2"), NO_API) \
	DECLARE_SERIALIZER(ASplat2Character) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splat2_Source_Splat2_Splat2Character_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASplat2Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASplat2Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplat2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplat2Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplat2Character(ASplat2Character&&); \
	NO_API ASplat2Character(const ASplat2Character&); \
public:


#define Splat2_Source_Splat2_Splat2Character_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplat2Character(ASplat2Character&&); \
	NO_API ASplat2Character(const ASplat2Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplat2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplat2Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASplat2Character)


#define Splat2_Source_Splat2_Splat2Character_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ASplat2Character, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ASplat2Character, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ASplat2Character, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ASplat2Character, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ASplat2Character, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ASplat2Character, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ASplat2Character, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ASplat2Character, L_MotionController); }


#define Splat2_Source_Splat2_Splat2Character_h_12_PROLOG
#define Splat2_Source_Splat2_Splat2Character_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splat2_Source_Splat2_Splat2Character_h_15_PRIVATE_PROPERTY_OFFSET \
	Splat2_Source_Splat2_Splat2Character_h_15_RPC_WRAPPERS \
	Splat2_Source_Splat2_Splat2Character_h_15_INCLASS \
	Splat2_Source_Splat2_Splat2Character_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Splat2_Source_Splat2_Splat2Character_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splat2_Source_Splat2_Splat2Character_h_15_PRIVATE_PROPERTY_OFFSET \
	Splat2_Source_Splat2_Splat2Character_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Splat2_Source_Splat2_Splat2Character_h_15_INCLASS_NO_PURE_DECLS \
	Splat2_Source_Splat2_Splat2Character_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Splat2_Source_Splat2_Splat2Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

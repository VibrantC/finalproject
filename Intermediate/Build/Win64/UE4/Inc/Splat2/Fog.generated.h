// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLAT2_Fog_generated_h
#error "Fog.generated.h already included, missing '#pragma once' in Fog.h"
#endif
#define SPLAT2_Fog_generated_h

#define Splat2_Source_Splat2_Fog_h_12_RPC_WRAPPERS
#define Splat2_Source_Splat2_Fog_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Splat2_Source_Splat2_Fog_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFog(); \
	friend SPLAT2_API class UClass* Z_Construct_UClass_AFog(); \
public: \
	DECLARE_CLASS(AFog, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splat2"), NO_API) \
	DECLARE_SERIALIZER(AFog) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splat2_Source_Splat2_Fog_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFog(); \
	friend SPLAT2_API class UClass* Z_Construct_UClass_AFog(); \
public: \
	DECLARE_CLASS(AFog, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splat2"), NO_API) \
	DECLARE_SERIALIZER(AFog) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splat2_Source_Splat2_Fog_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFog(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFog) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFog); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFog); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFog(AFog&&); \
	NO_API AFog(const AFog&); \
public:


#define Splat2_Source_Splat2_Fog_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFog(AFog&&); \
	NO_API AFog(const AFog&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFog); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFog); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFog)


#define Splat2_Source_Splat2_Fog_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__m_squarePlane() { return STRUCT_OFFSET(AFog, m_squarePlane); } \
	FORCEINLINE static uint32 __PPO__m_dynamicTexture() { return STRUCT_OFFSET(AFog, m_dynamicTexture); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterial() { return STRUCT_OFFSET(AFog, m_dynamicMaterial); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterialInstance() { return STRUCT_OFFSET(AFog, m_dynamicMaterialInstance); }


#define Splat2_Source_Splat2_Fog_h_9_PROLOG
#define Splat2_Source_Splat2_Fog_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splat2_Source_Splat2_Fog_h_12_PRIVATE_PROPERTY_OFFSET \
	Splat2_Source_Splat2_Fog_h_12_RPC_WRAPPERS \
	Splat2_Source_Splat2_Fog_h_12_INCLASS \
	Splat2_Source_Splat2_Fog_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Splat2_Source_Splat2_Fog_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splat2_Source_Splat2_Fog_h_12_PRIVATE_PROPERTY_OFFSET \
	Splat2_Source_Splat2_Fog_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Splat2_Source_Splat2_Fog_h_12_INCLASS_NO_PURE_DECLS \
	Splat2_Source_Splat2_Fog_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Splat2_Source_Splat2_Fog_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "ForestController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeForestController() {}
// Cross Module References
	SPLAT2_API UClass* Z_Construct_UClass_AForestController_NoRegister();
	SPLAT2_API UClass* Z_Construct_UClass_AForestController();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Splat2();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SPLAT2_API UClass* Z_Construct_UClass_AFoliageActor_NoRegister();
	SPLAT2_API UClass* Z_Construct_UClass_ARockActor_NoRegister();
	SPLAT2_API UClass* Z_Construct_UClass_ATreeActor_NoRegister();
// End Cross Module References
	void AForestController::StaticRegisterNativesAForestController()
	{
	}
	UClass* Z_Construct_UClass_AForestController_NoRegister()
	{
		return AForestController::StaticClass();
	}
	UClass* Z_Construct_UClass_AForestController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_Splat2,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "ForestController.h" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoliageFive_MetaData[] = {
				{ "Category", "Foliage five" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_FoliageFive = { UE4CodeGen_Private::EPropertyClass::Class, "FoliageFive", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, FoliageFive), Z_Construct_UClass_AFoliageActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_FoliageFive_MetaData, ARRAY_COUNT(NewProp_FoliageFive_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoliageFour_MetaData[] = {
				{ "Category", "Foliage four" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_FoliageFour = { UE4CodeGen_Private::EPropertyClass::Class, "FoliageFour", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, FoliageFour), Z_Construct_UClass_AFoliageActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_FoliageFour_MetaData, ARRAY_COUNT(NewProp_FoliageFour_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoliageThree_MetaData[] = {
				{ "Category", "Foliage three" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_FoliageThree = { UE4CodeGen_Private::EPropertyClass::Class, "FoliageThree", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, FoliageThree), Z_Construct_UClass_AFoliageActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_FoliageThree_MetaData, ARRAY_COUNT(NewProp_FoliageThree_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoliageTwo_MetaData[] = {
				{ "Category", "Foliage two" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_FoliageTwo = { UE4CodeGen_Private::EPropertyClass::Class, "FoliageTwo", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, FoliageTwo), Z_Construct_UClass_AFoliageActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_FoliageTwo_MetaData, ARRAY_COUNT(NewProp_FoliageTwo_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoliageOne_MetaData[] = {
				{ "Category", "Foliage one" },
				{ "ModuleRelativePath", "ForestController.h" },
				{ "ToolTip", "Foliage Actors" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_FoliageOne = { UE4CodeGen_Private::EPropertyClass::Class, "FoliageOne", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, FoliageOne), Z_Construct_UClass_AFoliageActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_FoliageOne_MetaData, ARRAY_COUNT(NewProp_FoliageOne_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreeSeven_MetaData[] = {
				{ "Category", "Scree seven" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_ScreeSeven = { UE4CodeGen_Private::EPropertyClass::Class, "ScreeSeven", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, ScreeSeven), Z_Construct_UClass_ARockActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_ScreeSeven_MetaData, ARRAY_COUNT(NewProp_ScreeSeven_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreeSix_MetaData[] = {
				{ "Category", "Scree six" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_ScreeSix = { UE4CodeGen_Private::EPropertyClass::Class, "ScreeSix", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, ScreeSix), Z_Construct_UClass_ARockActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_ScreeSix_MetaData, ARRAY_COUNT(NewProp_ScreeSix_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreeFive_MetaData[] = {
				{ "Category", "Scree Five" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_ScreeFive = { UE4CodeGen_Private::EPropertyClass::Class, "ScreeFive", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, ScreeFive), Z_Construct_UClass_ARockActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_ScreeFive_MetaData, ARRAY_COUNT(NewProp_ScreeFive_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreeFour_MetaData[] = {
				{ "Category", "Scree four" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_ScreeFour = { UE4CodeGen_Private::EPropertyClass::Class, "ScreeFour", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, ScreeFour), Z_Construct_UClass_ARockActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_ScreeFour_MetaData, ARRAY_COUNT(NewProp_ScreeFour_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreeThree_MetaData[] = {
				{ "Category", "Scree three" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_ScreeThree = { UE4CodeGen_Private::EPropertyClass::Class, "ScreeThree", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, ScreeThree), Z_Construct_UClass_ARockActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_ScreeThree_MetaData, ARRAY_COUNT(NewProp_ScreeThree_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreeTwo_MetaData[] = {
				{ "Category", "Scree two" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_ScreeTwo = { UE4CodeGen_Private::EPropertyClass::Class, "ScreeTwo", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, ScreeTwo), Z_Construct_UClass_ARockActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_ScreeTwo_MetaData, ARRAY_COUNT(NewProp_ScreeTwo_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreeOne_MetaData[] = {
				{ "Category", "Scree one" },
				{ "ModuleRelativePath", "ForestController.h" },
				{ "ToolTip", "Rock Actors" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_ScreeOne = { UE4CodeGen_Private::EPropertyClass::Class, "ScreeOne", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, ScreeOne), Z_Construct_UClass_ARockActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_ScreeOne_MetaData, ARRAY_COUNT(NewProp_ScreeOne_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TreeFive_MetaData[] = {
				{ "Category", "Tree Five" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_TreeFive = { UE4CodeGen_Private::EPropertyClass::Class, "TreeFive", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, TreeFive), Z_Construct_UClass_ATreeActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_TreeFive_MetaData, ARRAY_COUNT(NewProp_TreeFive_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TreeFour_MetaData[] = {
				{ "Category", "Tree Four" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_TreeFour = { UE4CodeGen_Private::EPropertyClass::Class, "TreeFour", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, TreeFour), Z_Construct_UClass_ATreeActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_TreeFour_MetaData, ARRAY_COUNT(NewProp_TreeFour_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TreeThree_MetaData[] = {
				{ "Category", "Tree Three" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_TreeThree = { UE4CodeGen_Private::EPropertyClass::Class, "TreeThree", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, TreeThree), Z_Construct_UClass_ATreeActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_TreeThree_MetaData, ARRAY_COUNT(NewProp_TreeThree_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TreeTwo_MetaData[] = {
				{ "Category", "Tree Two" },
				{ "ModuleRelativePath", "ForestController.h" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_TreeTwo = { UE4CodeGen_Private::EPropertyClass::Class, "TreeTwo", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, TreeTwo), Z_Construct_UClass_ATreeActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_TreeTwo_MetaData, ARRAY_COUNT(NewProp_TreeTwo_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TreeOne_MetaData[] = {
				{ "Category", "Tree one" },
				{ "ModuleRelativePath", "ForestController.h" },
				{ "ToolTip", "Tree Actors" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_TreeOne = { UE4CodeGen_Private::EPropertyClass::Class, "TreeOne", RF_Public|RF_Transient|RF_MarkAsNative, 0x0024080000010001, 1, nullptr, STRUCT_OFFSET(AForestController, TreeOne), Z_Construct_UClass_ATreeActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_TreeOne_MetaData, ARRAY_COUNT(NewProp_TreeOne_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_FoliageFive,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_FoliageFour,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_FoliageThree,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_FoliageTwo,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_FoliageOne,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreeSeven,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreeSix,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreeFive,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreeFour,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreeThree,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreeTwo,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreeOne,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TreeFive,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TreeFour,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TreeThree,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TreeTwo,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TreeOne,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AForestController>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AForestController::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AForestController, 2552286912);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AForestController(Z_Construct_UClass_AForestController, &AForestController::StaticClass, TEXT("/Script/Splat2"), TEXT("AForestController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AForestController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

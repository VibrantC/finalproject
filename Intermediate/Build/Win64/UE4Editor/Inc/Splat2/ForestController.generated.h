// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLAT2_ForestController_generated_h
#error "ForestController.generated.h already included, missing '#pragma once' in ForestController.h"
#endif
#define SPLAT2_ForestController_generated_h

#define Splat2_Source_Splat2_ForestController_h_16_RPC_WRAPPERS
#define Splat2_Source_Splat2_ForestController_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Splat2_Source_Splat2_ForestController_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAForestController(); \
	friend SPLAT2_API class UClass* Z_Construct_UClass_AForestController(); \
public: \
	DECLARE_CLASS(AForestController, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splat2"), NO_API) \
	DECLARE_SERIALIZER(AForestController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splat2_Source_Splat2_ForestController_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAForestController(); \
	friend SPLAT2_API class UClass* Z_Construct_UClass_AForestController(); \
public: \
	DECLARE_CLASS(AForestController, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splat2"), NO_API) \
	DECLARE_SERIALIZER(AForestController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splat2_Source_Splat2_ForestController_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AForestController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AForestController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AForestController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AForestController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AForestController(AForestController&&); \
	NO_API AForestController(const AForestController&); \
public:


#define Splat2_Source_Splat2_ForestController_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AForestController(AForestController&&); \
	NO_API AForestController(const AForestController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AForestController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AForestController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AForestController)


#define Splat2_Source_Splat2_ForestController_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TreeOne() { return STRUCT_OFFSET(AForestController, TreeOne); } \
	FORCEINLINE static uint32 __PPO__TreeTwo() { return STRUCT_OFFSET(AForestController, TreeTwo); } \
	FORCEINLINE static uint32 __PPO__TreeThree() { return STRUCT_OFFSET(AForestController, TreeThree); } \
	FORCEINLINE static uint32 __PPO__TreeFour() { return STRUCT_OFFSET(AForestController, TreeFour); } \
	FORCEINLINE static uint32 __PPO__TreeFive() { return STRUCT_OFFSET(AForestController, TreeFive); } \
	FORCEINLINE static uint32 __PPO__ScreeOne() { return STRUCT_OFFSET(AForestController, ScreeOne); } \
	FORCEINLINE static uint32 __PPO__ScreeTwo() { return STRUCT_OFFSET(AForestController, ScreeTwo); } \
	FORCEINLINE static uint32 __PPO__ScreeThree() { return STRUCT_OFFSET(AForestController, ScreeThree); } \
	FORCEINLINE static uint32 __PPO__ScreeFour() { return STRUCT_OFFSET(AForestController, ScreeFour); } \
	FORCEINLINE static uint32 __PPO__ScreeFive() { return STRUCT_OFFSET(AForestController, ScreeFive); } \
	FORCEINLINE static uint32 __PPO__ScreeSix() { return STRUCT_OFFSET(AForestController, ScreeSix); } \
	FORCEINLINE static uint32 __PPO__ScreeSeven() { return STRUCT_OFFSET(AForestController, ScreeSeven); } \
	FORCEINLINE static uint32 __PPO__FoliageOne() { return STRUCT_OFFSET(AForestController, FoliageOne); } \
	FORCEINLINE static uint32 __PPO__FoliageTwo() { return STRUCT_OFFSET(AForestController, FoliageTwo); } \
	FORCEINLINE static uint32 __PPO__FoliageThree() { return STRUCT_OFFSET(AForestController, FoliageThree); } \
	FORCEINLINE static uint32 __PPO__FoliageFour() { return STRUCT_OFFSET(AForestController, FoliageFour); } \
	FORCEINLINE static uint32 __PPO__FoliageFive() { return STRUCT_OFFSET(AForestController, FoliageFive); } \
	FORCEINLINE static uint32 __PPO__cubeActor() { return STRUCT_OFFSET(AForestController, cubeActor); }


#define Splat2_Source_Splat2_ForestController_h_13_PROLOG
#define Splat2_Source_Splat2_ForestController_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splat2_Source_Splat2_ForestController_h_16_PRIVATE_PROPERTY_OFFSET \
	Splat2_Source_Splat2_ForestController_h_16_RPC_WRAPPERS \
	Splat2_Source_Splat2_ForestController_h_16_INCLASS \
	Splat2_Source_Splat2_ForestController_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Splat2_Source_Splat2_ForestController_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splat2_Source_Splat2_ForestController_h_16_PRIVATE_PROPERTY_OFFSET \
	Splat2_Source_Splat2_ForestController_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Splat2_Source_Splat2_ForestController_h_16_INCLASS_NO_PURE_DECLS \
	Splat2_Source_Splat2_ForestController_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Splat2_Source_Splat2_ForestController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

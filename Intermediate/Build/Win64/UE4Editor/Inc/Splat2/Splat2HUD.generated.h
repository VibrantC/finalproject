// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLAT2_Splat2HUD_generated_h
#error "Splat2HUD.generated.h already included, missing '#pragma once' in Splat2HUD.h"
#endif
#define SPLAT2_Splat2HUD_generated_h

#define Splat2_Source_Splat2_Splat2HUD_h_12_RPC_WRAPPERS
#define Splat2_Source_Splat2_Splat2HUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Splat2_Source_Splat2_Splat2HUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASplat2HUD(); \
	friend SPLAT2_API class UClass* Z_Construct_UClass_ASplat2HUD(); \
public: \
	DECLARE_CLASS(ASplat2HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/Splat2"), NO_API) \
	DECLARE_SERIALIZER(ASplat2HUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splat2_Source_Splat2_Splat2HUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASplat2HUD(); \
	friend SPLAT2_API class UClass* Z_Construct_UClass_ASplat2HUD(); \
public: \
	DECLARE_CLASS(ASplat2HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/Splat2"), NO_API) \
	DECLARE_SERIALIZER(ASplat2HUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splat2_Source_Splat2_Splat2HUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASplat2HUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASplat2HUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplat2HUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplat2HUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplat2HUD(ASplat2HUD&&); \
	NO_API ASplat2HUD(const ASplat2HUD&); \
public:


#define Splat2_Source_Splat2_Splat2HUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplat2HUD(ASplat2HUD&&); \
	NO_API ASplat2HUD(const ASplat2HUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplat2HUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplat2HUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASplat2HUD)


#define Splat2_Source_Splat2_Splat2HUD_h_12_PRIVATE_PROPERTY_OFFSET
#define Splat2_Source_Splat2_Splat2HUD_h_9_PROLOG
#define Splat2_Source_Splat2_Splat2HUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splat2_Source_Splat2_Splat2HUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Splat2_Source_Splat2_Splat2HUD_h_12_RPC_WRAPPERS \
	Splat2_Source_Splat2_Splat2HUD_h_12_INCLASS \
	Splat2_Source_Splat2_Splat2HUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Splat2_Source_Splat2_Splat2HUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splat2_Source_Splat2_Splat2HUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Splat2_Source_Splat2_Splat2HUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Splat2_Source_Splat2_Splat2HUD_h_12_INCLASS_NO_PURE_DECLS \
	Splat2_Source_Splat2_Splat2HUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Splat2_Source_Splat2_Splat2HUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
